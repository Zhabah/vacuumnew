﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class BacktoMenu : MonoBehaviour {

	public Button backToMenu;
	[FMODUnity.EventRef]
	public string buttonClickSound;
	// Use this for initialization
	void Start () {
		Button button = backToMenu.GetComponent<Button>();
		button.onClick.AddListener(TaskOnClick);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TaskOnClick()
	{
		FMODUnity.RuntimeManager.PlayOneShot (buttonClickSound, transform.position);
		StartCoroutine ("MenuReturn");
	}

	IEnumerator MenuReturn ()
	{
		yield return new WaitForSeconds(1);
		SceneManager.LoadScene ("Menu");
	}
}



