﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorChange : MonoBehaviour {
	

	private SpriteRenderer spriteRenderer;




	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();

	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "vacuum") {
			StartCoroutine (FadeOut (true));
		}
	}

	IEnumerator FadeOut (bool FadeAway)
	{
		if(FadeAway)
		{
			for (float i = 1; i >= 0; i -= Time.deltaTime)
			{
				spriteRenderer.color = new Color (1, 1, 1, i);
				yield return null;
		}
	}
}
}
