﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VacuumMovement : MonoBehaviour {

	private float moveSpeed;
	public Rigidbody2D vacuum;
	public float upSpeed;
	public float downSpeed;
	public float slowSpeed;
	public float stallSpeed;
	public float initialSpeed;
	private CleanlinessManager theCleanlinessManager;
	public GameObject smoke1;
	public GameObject smoke2;
	public GameObject smoke3;
	public GameObject smoke4;
	public Image smokeScreen;
	[FMODUnity.EventRef]
	public string VacuumUpEvent;
	public FMOD.Studio.EventInstance vacuumUp;






	void Start () {
		theCleanlinessManager = FindObjectOfType<CleanlinessManager>();
		vacuum = GetComponent<Rigidbody2D>();
		moveSpeed = initialSpeed;
		vacuumUp = FMODUnity.RuntimeManager.CreateInstance(VacuumUpEvent);
		vacuumUp.start();
	
	
	}
	
	// Update is called once per frame
	void Update () {
		if (theCleanlinessManager.GetComponent<CleanlinessManager> ().gameActive == true) {
			if (Input.GetButton ("Jump")) {
				vacuum.velocity = new Vector3 (moveSpeed, downSpeed, 0);
			} else {
				vacuum.velocity = new Vector3 (moveSpeed, upSpeed, 0);
				vacuumUp.setParameterValue ("vacuumUpDown", 1f);

			}
		} else {
			vacuum.velocity = new Vector3 (0, 0, 0);
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "stall") {
			

			StartCoroutine ("Stall");
			StartCoroutine ("Smoke");

		}

		if(other.tag == "slow"){
			StartCoroutine ("Slow");
		
		}
		if (other.tag == "floor") {
			vacuumUp.setParameterValue ("vacuumUpDown", 0f);
		}
	}

	IEnumerator Stall ()
	{
		moveSpeed = stallSpeed;

		yield return new WaitForSeconds (1.0f);
		moveSpeed = initialSpeed;
		Debug.Log ("stall");
	}

	IEnumerator Smoke()
	{
		smokeScreen.gameObject.SetActive (true);
		smoke1.gameObject.SetActive(true);
		smoke2.gameObject.SetActive(true);
		yield return new WaitForSeconds (.7f);
		smoke1.gameObject.SetActive (false);	
		smoke2.gameObject.SetActive (false);
		smoke3.gameObject.SetActive(true);
		smoke4.gameObject.SetActive(true);
		yield return new WaitForSeconds (.7f);
		smoke3.gameObject.SetActive (false);	
		smoke4.gameObject.SetActive (false);
		smoke1.gameObject.SetActive(true);
		smoke2.gameObject.SetActive(true);

		smoke1.gameObject.SetActive (false);	
		smoke2.gameObject.SetActive (false);
		smokeScreen.gameObject.SetActive (false);

	}

	IEnumerator Slow()
	{
		moveSpeed = slowSpeed;
		yield return new WaitForSeconds (2.0f);
		moveSpeed = initialSpeed;
		Debug.Log ("slow");
	}
}

