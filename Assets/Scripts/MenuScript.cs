﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuScript : MonoBehaviour {
	
	public Rigidbody2D vacuum;
	public Button beginGame;
	[FMODUnity.EventRef]
	public string buttonSound;

	// Use this for initialization
	void Start () {
		Button button = beginGame.GetComponent<Button>();
		button.onClick.AddListener(TaskOnClick);
		VacuumRoll ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void VacuumRoll()
	{
		vacuum.velocity = new Vector3 (8, 0, 0);
	}

	public void TaskOnClick()
	{
		FMODUnity.RuntimeManager.PlayOneShot (buttonSound, transform.position);
		StartCoroutine ("Begin");
	}

	IEnumerator Begin()
	{yield return new WaitForSeconds (1);
	SceneManager.LoadScene ("LivingLevel");
	}
}
