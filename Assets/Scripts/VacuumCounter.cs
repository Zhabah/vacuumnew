﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VacuumCounter : MonoBehaviour {

	public int One;
	private CleanlinessManager theCleanlinessManager;

	// Use this for initialization
	void Start () {
		theCleanlinessManager = FindObjectOfType<CleanlinessManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "floor") {
			theCleanlinessManager.AddOne (One);
		
		}
	}
}
